﻿namespace CurveAdjustPlugin
{
    partial class CurveControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // CurveControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Cursor = System.Windows.Forms.Cursors.Cross;
            this.Name = "CurveControl";
            this.Size = new System.Drawing.Size(278, 265);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.CurveControl_Paint);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.CurveControl_MouseMove);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.CurveControl_MouseDown);
            this.Resize += new System.EventHandler(this.CurveControl_Resize);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.CurveControl_MouseUp);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CurveControl_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Sbk_Manage
{
    public partial class FrmAuth : Form
    {
        public FrmAuth()
        {
            InitializeComponent();
            PasswordTextBox.KeyPress += new KeyPressEventHandler(PasswordTextBox_KeyPress);
        }
        private void PasswordTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(System.Windows.Forms.Keys.Enter))
            {
                OK_Click(null, null);
            }
        }
        private void OK_Click(object sender, EventArgs e)
        {
            if (this.PasswordTextBox.Text.Trim().Equals(Program.User_Pwd))
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("非法操作!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                PasswordTextBox.Focus();
                return;
            }
        }

        private void FrmAuth_Load(object sender, EventArgs e)
        {
            this.KeyPreview = true;
        }
    }
}
